/*
* LibreCheck - free check-in software | Electron Front-end
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// This file contains extra JavaScript that does not have to do with the checkin system, but with the navigation of the client and other misc functions.

'use strict';

function openPopup(element) {
    document.getElementById(element).style.display = 'block';
    document.getElementById('backdrop').style.display = 'block';
}
function closePopup(element) {
    document.getElementById(element).style.display = 'none';
    document.getElementById('backdrop').style.display = 'none';
}
function closePopups() {
    let popups = document.getElementsByClassName('popup');
    for (let i = 0; i < popups.length; i++) {
        popups[i].style.display = 'none';
    }
    document.getElementById('backdrop').style.display = 'none';
}


//// Keypress code derived from https://stackoverflow.com/a/7601253 by Maxx
// Enter with enter key
document.getElementById('phone').onkeydown = function (key) {
    if (key.keyCode == 13) {
        searchPhone();
    }
}

// Exit popups with escape key
document.onkeydown = function (key) {
    if (key.keyCode == 27) {
        closePopups();
    }
}


// Select all function for CheckIn Popup.

function toggleSelect(el) {
    if (document.getElementById('selectAll_' + el).checked == true) {
        toggleSelectAll(el, true);
    } else {
        toggleSelectAll(el, false);
    }
}

function toggleSelectAll(el, option) {
    let elements = document.getElementById(el).getElementsByTagName('div');
    // Grab checkboxes
    let checkboxes = [];
    for (let i = 0; i < elements.length; i++) {
        checkboxes.push(elements[i].getElementsByTagName('input')[0]);
    }

    // Select/deselect all checkboxes
    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = option;
    }
}