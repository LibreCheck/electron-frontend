/*
* LibreCheck - free check-in software | Electron Front-end
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

'use strict';

const request = require('request');

let places = [];
getPlaces();

// document.getElementById('checkinButton').addEventListener('click', checkIn());

/*----
Checking in/out people code:
----*/

function toggleCheck(func) {
    // First clear phone number input box
    document.getElementById('phone').value = '';
    // Select check in box for easy entry again
    document.getElementById('phone').select();
    
    // Then prepare variables for use
    let peopleEl;
    let place = [];
    let url;

    // Use different settings if using checkin or checkout
    if (func == 'checkin') {
        peopleEl = document.getElementById('notChecked').getElementsByTagName('div');

        for (let i = 0; i < peopleEl.length; i++) {
            const element = peopleEl[i].getElementsByTagName('select')[0];

            place[place.length] = element.value;
        }
        url = 'http://localhost:8100/checkin.php';
    } else {
        peopleEl = document.getElementById('areChecked').getElementsByTagName('div');
        url = 'http://localhost:8100/checkout.php';
    }
    
    let who = [];

    for (let i = 0; i < peopleEl.length; i++) {
        const element = peopleEl[i].getElementsByTagName('input')[0];
        
        if (element.checked == true) {
            who[who.length] = element.id;
            
        }
    }

    console.log('People and places:')
    console.log(who);
    console.log(places);

    let formData = {
        people: JSON.stringify(who),
        places: JSON.stringify(place)
    };
    console.log(formData);
    request.post({url: url, formData: formData}, function optionalCallback(error, response, body) {
        if (error == null) {
            console.log(body);
            
            closePopup('checkin');
            
        } else {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);
            document.getElementById()
        }
    });
}

// Function for creating elements
function checkinElement(itemName, data) {
    let itemEl = document.getElementById(itemName);
    itemEl.innerHTML = '';
    for (var i = 0; i < data.length; i++) {
        let wrapper = document.createElement('div');
        let itemItem = document.createElement('div');
        let info = document.createElement('span');
        let checkbox = document.createElement('input');
        let selectbox;

        checkbox.type = 'checkbox';
        checkbox.id = data[i][1];

        info.innerHTML = data[i][2] + ' ' + data[i][3] + ' | Guardian: ' + data[i][4] + ' ';
        itemItem.appendChild(checkbox);
        itemItem.appendChild(info);

        // Add places selecter for notChecked request
        if (itemName == 'notChecked') {
            selectbox = document.createElement('select');
            for (let i = 0; i < places.length; i++) {
                const element = places[i];
                let placeEl = document.createElement('option');
                placeEl.value = element;
                placeEl.innerText = places[i];
                selectbox.append(placeEl);
            }
            itemItem.appendChild(selectbox);
        }
        wrapper.appendChild(itemItem);
        //itemItem.id = data[i][1];
        itemEl.innerHTML = wrapper.innerHTML + itemEl.innerHTML;
    }
}

// Load the check-in popup
function loadCheckin(data) {
    toggleSelect('areChecked');
    toggleSelect('notChecked');
    document.getElementById('selectAll_notChecked').checked = false;
    document.getElementById('selectAll_areChecked').checked = false;

    console.log('The following people were retrieved:');
    console.log(data);
    let all = data.all;
    let checked = data.checked;
    //console.log('These are checked in:');
    //console.log(checked);
    let notChecked = [];

    // See if certain people are checked in one by one
    for (let i = 0; i < all.length; i++) {
        const person = all[i];
        let isChecked = false;
        for (let index = 0; index < checked.length; index++) {
            const checkedPerson = checked[index];
            // Compare IDs
            if (checkedPerson[1] == person[1]) {
                isChecked = true;
            }
            //console.log(checkedPerson[1] + ' ' + person[1]);
            
        }
        if (isChecked == false) {
            notChecked.push(person);
        }
    }
    //console.log('These are not checked in');
    //console.log(notChecked);
    

    document.getElementById('phonenum').innerText = 'People linked to the number ' + all[0][0] + ':';

    openPopup('checkin');

    checkinElement('notChecked', notChecked);
    checkinElement('areChecked', checked);
    
}

function searchPhone() {
    let num = document.getElementById('phone').value;
    console.log(num);
    
    let formData = {
        phone: num
    };
    request.post({url:'http://localhost:8100/lookup.php', formData: formData}, function optionalCallback(error, response, body) {
        if (error == null) {
            //console.log(body);
            
            loadCheckin(JSON.parse(body));
        } else {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);
        }
    });
}


/*----
Adding new user code:
----*/

let children = [];

function openAddUser() {
    children = [];
    openPopup('adduser');
}

function removeCommas(text) {
    text = text.split(',').join(' ');
    return text;
}

function addUser() {
    let phone = document.getElementById('adduser_phone').value;
    let guardName = document.getElementById('adduser_guardfirstname').value + ' ' + document.getElementById('adduser_guardlastname').value;
    
    let formData = {
        "phone": phone,
        "guardname": guardName,
        "children": JSON.stringify(children) 
    }

    request.post({ url: 'http://localhost:8100/add-person.php', formData: formData }, function optionalCallback(error, response, body) {
        if (error == null) {
            closePopup('adduser');
            console.log(JSON.parse(body));
            

        } else {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);
        }
    });
}

function addKid() {
    let first = document.getElementById('adduser_childfirstname').value;
    let last = document.getElementById('adduser_childlastname').value;
    let birthday = document.getElementById('adduser_childbirthday').value;
    let gender = document.getElementById('adduser_childgender').value;
    let medical = document.getElementById('adduser_childmedical').value;
    if (medical == '') {
        medical = 'none';
    }
    // Remove commas because they can be a problem in the people.csv file
    medical = removeCommas(medical);
    // Add this information as a child in the request
    children.push(
        {
            "first": first,
            "last": last,
            "dob": birthday,
            "gender": gender,
            "medical": medical
        }
    );
    console.log(children);

    // Display notification about the child being added, and empty after 3 seconds.
    document.getElementById('adduser_added').innerText = 'Added ' + first + ' ' + last + ' to your request.';
    setTimeout(() => {
        document.getElementById('adduser_added').innerText = '';
    }, 3000);
    
    // Reset some of the fields
    document.getElementById('adduser_childfirstname').value = '';
    document.getElementById('adduser_childbirthday').value = '';
    document.getElementById('adduser_childmedical').value = '';
}

/*----
Grab available places code:
----*/

function getPlaces() {
    request.post({ url: 'http://localhost:8100/getplaces.php'}, function optionalCallback(error, response, body) {
        if (error == null) {
            console.log(JSON.parse(body));
            places = JSON.parse(body);
        } else {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);
        }
    });
}

/*----
All People popup code:
----*/

function loadPeopleEl(data) {
    let itemEl = document.getElementById('people');
    itemEl.innerHTML = '';
    for (var i = 0; i < data.length; i++) {
        let wrapper = document.createElement('div');
        let itemItem = document.createElement('div');
        let info = document.createElement('span');

        info.innerHTML = data[i][2] + ' ' + data[i][3] + ' | Guardian: ' + data[i][4] + ' | Phone: ' + data[i][0] + ' | Medical/allergies: ' + data[i][7];
        itemItem.appendChild(info);

        wrapper.appendChild(itemItem);
        itemEl.innerHTML = wrapper.innerHTML + itemEl.innerHTML;
    }
}

function openAllPeople() {
    openPopup('allpeople');

    request.post({url:'http://localhost:8100/getall.php'}, function optionalCallback(error, response, body) {
        console.log(JSON.parse(body));
        let data = JSON.parse(body);
        let all = data.all;
        let checked = data.checked;
        let notChecked = [];
        if (error == null) {
            for (let i = 0; i < all.length; i++) {
                const person = all[i];
                let isChecked = false;
                for (let index = 0; index < checked.length; index++) {
                    const checkedPerson = checked[index];
                    // Compare IDs
                    if (checkedPerson[1] == person[1]) {
                        isChecked = true;
                    }
                    //console.log(checkedPerson[1] + ' ' + person[1]);
                    
                }
                if (isChecked == false) {
                    notChecked.push(person);
                }
            }

            // Load all who are checked in
            loadPeopleEl(checked);
            
        } else {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('body:', body);
        }
    });
}