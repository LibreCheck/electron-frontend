/*
* LibreCheck - free check-in software | Electron Front-end
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// This settings file is for client side configuration of the Electron front-end, and sending requests to the back-end to change their settings

'use strict';

const Store = require('electron-store');
let store = new Store();

function updateLocalSetting(id) {
    let data = document.getElementById(id).value;
    // Looks at all possible functions and chooses one
    switch(id) {
        case 'background-image':
        store.set('')
            updateBackground(data);
    }
    
}


// Specific client-side changes

function updateBackground(background) {
    console.log('Changing background!');
    
    document.body.style.backgroundImage = 'url(images/' + background +')';
}