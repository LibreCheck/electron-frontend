# LibreCheck Electron Front-end

Use yarn or npm.
```
git clone https://gitlab.com/librecheck/electron-frontend
yarn install
yarn start
```

## Copyright
Copyright (c) 2018, 2019 MasterOfTheTiger.
This program is licensed under AGPL-3.0-or-later.